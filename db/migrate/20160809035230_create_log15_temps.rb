class CreateLog15Temps < ActiveRecord::Migration
  def change
    create_table :log15_temps do |t|
      t.string :zip_code
      t.float :temp_min
      t.float :temp_max

      t.timestamps null: false
    end
  end
end
