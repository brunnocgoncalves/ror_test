class Log15Temp < ActiveRecord::Base
	validates :zip_code, presence: true
	validates :zip_code, length: { minimum: 3 }
end
