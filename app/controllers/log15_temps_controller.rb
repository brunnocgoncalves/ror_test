require 'httparty'

class Log15TempsController < ApplicationController
  before_action :set_log15_temp, only: [:show, :edit, :update, :destroy]

  # GET /log15_temps
  # GET /log15_temps.json
  def index
    @log15_temps = Log15Temp.all
  end

  # GET /log15_temps/1
  # GET /log15_temps/1.json
  def show
  end

  # GET /log15_temps/new
  def new
    @log15_temp = Log15Temp.new
  end

  # GET /log15_temps/1/edit
  def edit
  end

  # POST /log15_temps
  # POST /log15_temps.json
  def create
    @log15_temp = Log15Temp.new(log15_temp_params)
    if !getTemp
      @log15_temp.temp_min = 10
      @log15_temp.temp_max = 15
    end

    respond_to do |format|
      if @log15_temp.save
        format.html { redirect_to @log15_temp, notice: 'Log15 temp was successfully created.' }
        format.json { render :show, status: :created, location: @log15_temp }
      else
        format.html { render :new }
        format.json { render json: @log15_temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /log15_temps/1
  # DELETE /log15_temps/1.json
  def destroy
    @log15_temp.destroy
    respond_to do |format|
      format.html { redirect_to log15_temps_url, notice: 'Log15 temp was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def getTemp
    options = {headers:{ token:"DpMSKByXNrvNQYQNKVONescUifBvTNRZ" }}

    date = (Date.today-15).to_s
    zip_code = @log15_temp.zip_code

    url = "http://www.ncdc.noaa.gov/cdo-web/api/v2/data?datasetid=GHCND&locationid=ZIP:#{zip_code}&startdate=#{date}&enddate=#{date}&units=metric"

    response = HTTParty.get(url, options)

    if response.parsed_response["results"].nil?
      @log15_temp.temp_min = 0
      @log15_temp.temp_max = 0
    else
      for result in response.parsed_response["results"]
        if result["datatype"] == "TMIN"
          @log15_temp.temp_min = result["value"]
        elsif result["datatype"] == "TMAX"
          @log15_temp.temp_max = result["value"]
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log15_temp
      @log15_temp = Log15Temp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def log15_temp_params
      params.require(:log15_temp).permit(:zip_code, :temp_min, :temp_max)
    end
end
