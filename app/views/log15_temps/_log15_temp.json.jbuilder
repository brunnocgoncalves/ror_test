json.extract! log15_temp, :id, :zip_code, :temp_min, :temp_max, :created_at, :updated_at
json.url log15_temp_url(log15_temp, format: :json)